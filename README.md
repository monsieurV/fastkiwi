# README #

### What is this repository for? ###

* This repository is a simple 2d graphical engine using canvas.
* It currently manage : 
    - frame
    - inputs
    - events
    - resources loading
    - screen (full screen, resizing, ratio, ...)
    - some GUI element (button)
    - some widget (curve in orthonormal landmark).

### How do I get set up? ###
* This library use [RequireJS](http://www.requirejs.org/) to manage dependencies.
* you can generate the documentation using [jsdoc](http://usejsdoc.org/).

### Future ###
* It will manage some more things:
   - some more GUI element
   - some game helper like sprites / animated sprites / ...
   - some more things
/*global window, requirejs, require, define*/

define(['fastkiwi/gui/widget'], function (Widget) {
    'use strict';

    /**
     * A Button.
     *
     * @name Button
     * @constructor
     * @augment Widget
     * @this {Button}
     * @param {Number} x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} zindex The zindex of the widget.
     * @param {Number} width The width of the widget.
     * @param {Number} height The height of the widget.
     * @property {Number} x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @property {Number} y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @property {Number} zindex The zindex of the widget.
     * @property {Number} width The width of the widget.
     * @property {Number} height The height of the widget.
     * @property {Image} image The button sprite image.
     * @property {Object} states An itterative object containing all button states and their informations.
     * @property {String} currentState The name of the currant state of the button.
     * @property {Boolean} isEnable Is the button enable?
     * @property {Boolean} isTogglable Is the button Togglable?
     * @property {Boolean} isEngaged Is the button Engaged? It's used only if the button is togglable.
     * @property {Number} sx The x state coordinate where to start drawing button.
     * @property {Number} sy The y state coordinate where to start drawing button.
     * @property {Number} imageWidth The width of the button in the image.
     * @property {Number} imageHeight The height of the button in the image.
     * @property {Boolean} mouseDown Is the mouse down?
     */
    function Button(x, y, zindex, width, height) {
        Widget.apply(this, [x, y, zindex, width, height]);

        this.image          = undefined;
        this.states         = undefined;
        this.currentState   = '';
        this.isEnable       = true;
        this.isTogglable    = false;
        this.isEngaged      = false;
        this.sx             = 0;
        this.sy             = 0;
        this.imageWidth     = this.width;
        this.imageHeight    = this.height;
        this.mouseDown      = false;
    }

    Button.prototype = Object.create(Widget.prototype);

    //tmp laisser le choix de l'utiliser ou non / du niveau de transparance min / ou d'utiliser un mappage du widget via image.
    // ne pas recreer le buffer à chaque event ...
    Button.prototype.isInWidget = function (coordinate) {
        var minOpacity = 20;//tmp

        if (coordinate.x >= this.x && coordinate.x <= this.x + this.width &&
                coordinate.y >= this.y && coordinate.y <= this.y + this.height) {

            var canvas = window.document.createElement("canvas");
            canvas.width = this.width;
            canvas.height = this.height;
            var temp_context = canvas.getContext("2d");
            temp_context.drawImage(this.image, this.sx * this.imageWidth, this.sy * this.imageHeight, this.imageWidth, this.imageHeight, 0, 0, this.width, this.height);

            var imageData = temp_context.getImageData(0, 0, this.width, this.height).data;
            var imageClickPosition = {
                x : coordinate.x - this.x,
                y : coordinate.y - this.y
            };

            var index = imageClickPosition.x * 4 + imageClickPosition.y * this.width * 4;

            if (imageData[index + 3] > minOpacity) {
                return true;
            }
        }

        return false;
    };


    /**
     * Disable the button.
     *
     * @this {Button}
     */
    Button.prototype.disable = function () {
        this.isEnable = false;

        this.setState('inactive');

        if (this.isTogglable) {
            this.trigger('toggle', false);
        }
    };

    /**
     * Display the button.
     *
     * @this {Button}
     * @param {Context} context The context in which to draw the widget.
     */
    Button.prototype.display = function (context) {
        context.drawImage(this.image, this.sx * this.imageWidth, this.sy * this.imageHeight, this.imageWidth, this.imageHeight, this.x, this.y, this.width, this.height);
    };

    /**
     * Enable the button.
     *
     * @this {Button}
     */
    Button.prototype.enable = function () {
        this.isEnable = true;

        this.setState('base');

        if (this.isTogglable) {
            this.isEngaged = false;
        }
    };

    /**
     * Init the button.
     *
     * @this {Button}
     * @param {Object} data The init data.
     * @param {Number} data.x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} data.y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} data.zindex The zindex of the widget.
     * @param {Number} data.width The width of the widget.
     * @param {Number} data.height The height of the widget.
     * @param {Image} data.image The height of the widget.
     * @param {Object} data.states The height of the widget.
     */
    Button.prototype.init = function (data) {
        this.image          = (data.imageData === undefined) ? this.image : data.imageData;
        this.states         = (data.states    === undefined) ? this.states : data.states;
        this.imageHeight    = (data.height    === undefined) ? this.imageHeight : data.height;
        this.imageWidth     = (data.width    === undefined) ? this.imageWidth : data.width;

        data.width  = (this.width == 0) ? data.width : this.width;
        data.height = (this.height == 0) ? data.height : this.height;

        new Widget().init.apply(this, [data]);

        if (!this.states.hasOwnProperty('base')) {
            this.states.base = {
                x : 0,
                y : 0
            };
        }

        this.setState('base');
    };

    /**
     * The action to do when a mouse button is down.
     *
     * @this {Button}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Button.prototype.onMouseDown = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            if (e.button === 0) {
                this.mouseDown = true;
                if (this.isTogglable && this.isEngaged) {
                    this.setState('base');
                } else {
                    this.setState('click');
                }
            }

            return false;
        }

        this.setToDefaultState();
        return true;
    };

    /**
     * The action to do when a mouse button is move.
     *
     * @this {Button}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Button.prototype.onMouseMove = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            if (this.currentState !== 'click' || this.isTogglable) {
                this.setToHoverState();
            }

            return false;
        }

        this.setToDefaultState();
        return true;
    };

    /**
     * The action to do when a mouse button is up.
     *
     * @this {Button}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Button.prototype.onMouseUp = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            if (e.button === 0 && this.mouseDown) {
                if (this.isTogglable && this.isEnable) {
                    this.toggle();
                } else {
                    this.setToHoverState();

                    if (this.isEnable) {
                        this.trigger('click');
                    }
                }
            }

            return false;
        }

        this.mouseDown = false;
        this.setToDefaultState();

        return true;
    };

    /**
     * Modify the current button state. It trigger the state_changed event with the new state name in parameter.
     *
     * @this {Button}
     * @param {String} state The new button state.
     */
    Button.prototype.setState = function (state) {
        var previousState = this.currentState;

        if (!this.isEnable) {
            state = 'inactive';
        }

        if (!this.states.hasOwnProperty(state)) {
            state = 'base';
        }

        this.sx        = this.states[state].x;
        this.sy        = this.states[state].y;
        this.currentState    = state;

        if(previousState !== this.currentState) {
            this.trigger('state_changed', this.currentState);
        }
    };

    /**
     * Set the current button state to default.
     *
     * @this {Button}
     */
    Button.prototype.setToDefaultState = function () {
        if (this.isTogglable && this.isEngaged) {
            this.setState('click');
        } else {
            this.setState('base');
        }
    };

    /**
     * Transphorme the button to a Standard / Togglable Button.
     *
     * @this {Button}
     * @param {Boolean} [togglable=true] The new button state.
     */
    Button.prototype.setTogglable = function (togglable) {
        this.isTogglable = (typeof togglable !== 'boolean') ? true : togglable;
    };

    /**
     * Set the current button state to hover.
     *
     * @this {Button}
     */
    Button.prototype.setToHoverState = function () {
        if (this.states.hasOwnProperty('hover')) {
            this.setState('hover');
        } else {
            this.setToDefaultState();
        }
    };

    /**
     * Toggle the button.
     *
     * @this {Button}
     * @param {Boolean} [engaged=!this.isEngaged] The new button toggle state.
     */
    Button.prototype.toggle = function (engaged) {
        this.isEngaged = (typeof engaged !== 'boolean') ? !this.isEngaged : engaged;

        if (this.isEngaged) {
            this.setState('click');
        } else {
            this.setState('base');
        }

        this.trigger('toggle', this.isEngaged);
    };

    return Button;
});
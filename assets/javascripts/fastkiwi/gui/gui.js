/*global window, requirejs, require, define*/

define(['fastkiwi/multipleelementsmanager', 'fastkiwi/screenmanager'], function (MultipleElementsManager, ScreenManager) {
    'use strict';

    /**
     * Manage and display widgets.
     *
     * @name Gui
     * @constructor
     * @augments MultipleElementsManager
     * @this {Gui}
     * @param {Context} context The context used by the graphic engine.
     * @property {Context} context The context used by the graphic engine.
     * @property {Canvas} canvasBuffer The canvas used to prepare the gui drawing. It's not directly displayed.
     * @property {Context} contextBuffer The context used to prepare the gui drawing.
     * @property {Number} x The x-coordinate of the gui in the canvas. 0 by default.
     * @property {Number} y The y-coordinate of the gui in the canvas. 0 by default.
     * @property {Number} height The height of the gui. Canvas height by default.
     * @property {Number} width The width of the gui. Canvas width by default.
     * @property {Boolean} refreshDisplay The display must be refreshed?
     */
    function Gui(context, screenManager) {
        MultipleElementsManager.apply(this);

        this.screenManager  = screenManager;
        this.context        = context;
        this.canvasBuffer   = window.document.createElement('canvas');
        this.contextBuffer  = this.canvasBuffer.getContext('2d');
        this.x              = 0;
        this.y              = 0;
        this.height         = this.canvasBuffer.height  = context.canvas.height;
        this.width          = this.canvasBuffer.width   = context.canvas.width;
        this.canvasScale    = 1;
        this.refreshDisplay = false;
    }

    Gui.prototype = Object.create(MultipleElementsManager.prototype);

    /**
    * Get the event mouse position relative to the canvas. 
    *
    * @this {InputsManager}
    * @param {Event} e The current event object.
    */
    Gui.prototype.display = function () {
        if (this.refreshDisplay) {
            this.refreshDisplay     = false;
            this.context.fillStyle  = "rgb(255, 255, 255)";
            this.context.fillRect(0, 0, this.width, this.height);

            var widgets = this.getAllSortedBy('zindex'),
                widgetsQuantity = widgets.length,
                zindex;

            this.contextBuffer.clearRect(0, 0, this.width, this.height);

            for (zindex = 0; zindex < widgetsQuantity; zindex += 1) {
                widgets[zindex].display(this.contextBuffer);
            }

            this.context.drawImage(this.canvasBuffer, this.x, this.y);
        }
    };

    /**
    * Init the gui. Must be called before using gui functionality.
    *
    * @this {InputsManager}
    * @param {Boolean} enableCheckZindexModification If true, it enable the checking of widget zindexmodification.
    */
    Gui.prototype.init = function (enableCheckZindexModification) {
        var self = this;

        enableCheckZindexModification = (typeof enableCheckZindexModification !== 'boolean') ? true : enableCheckZindexModification;
        this.cacheArraySortedBy('zindex', true, enableCheckZindexModification);

        this.addEventListener('element_added', function (widgetName) {
            var widget = self.get(widgetName);

            widget.addEventListener('moved', function () {
                self.refreshDisplay = true;
            });

            widget.addEventListener('state_changed', function () {
                self.refreshDisplay = true;
            });

            self.refreshDisplay = true;
        });

        this.addEventListener('element_removed', function () {
            self.refreshDisplay = true;
        });

        this.screenManager.addEventListener("on_scale_changed", function(scale) {
            var scale_factor = scale / self.canvasScale;
            self.x      = Math.round(self.x * scale_factor);
            self.y      = Math.round(self.y * scale_factor);
            self.width  = Math.round(self.width * scale_factor);
            self.height = Math.round(self.height * scale_factor);
            self.canvasScale = scale;

            var widgets = self.getAllSortedBy('zindex', false),
                widgetsQuantity = widgets.length,
                zindex;

            for (zindex = 0; zindex < widgetsQuantity; zindex += 1) {
                widgets[zindex].onScaleChanged(scale_factor);
            }

            self.canvasBuffer.width  = self.context.canvas.width;
            self.canvasBuffer.height  = self.context.canvas.height;
        });

        this.screenManager.addEventListener("full_document_toggled", function(scale) {
            self.refreshDisplay = true;
        });

        this.screenManager.addEventListener("full_screen_toggled", function(scale) {
            self.refreshDisplay = true;
        });
    };

    /**
    * propage user inputs events in the widgets tree of the gui. 
    *
    * @this {InputsManager}
    * @param {Event} e the current event object.
    * @param {String} device The name of the device causing the event ('Mouse' or 'Key').
    * @param {String} type The name of the curent event type ('Up', 'Down', 'Move' or 'Wheel').
    * @param {Boolean} propageEvent Is the event stop by a widget?
    * @param {Object} canvasMouseCoordinate A literal objet contening the mouse position relative to the canvas.
    * @param {Number} canvasMouseCoordinate.x The x-coordinate of the mouse position.
    * @param {Number} canvasMouseCoordinate.y The y-coordinate of the mouse position.
    */
    Gui.prototype.propage = function (e, device, type, propageEvent, canvasMouseCoordinate) {
        var widgets = this.getAllSortedBy('zindex', false),
            widgetsQuantity = widgets.length,
            zindex;

        for (zindex = 0; zindex < widgetsQuantity; zindex += 1) {
            propageEvent = widgets[zindex]['on' + device + type](e, propageEvent, canvasMouseCoordinate) && propageEvent;
        }

        return propageEvent;
    };

    return Gui;
});
/*global window, requirejs, require, define*/

define(['fastkiwi/gui/widget', 'fastkiwi/gui/orthonormallandmark'], function (Widget, OrthonormalLandmark) {
    'use strict';

    /**
     * An curve // tmp ?? to rename
     */
    function Curve(x, y, zindex, width, height) {
        Widget.apply(this, [x, y, zindex, width, height]);
        this.xData = [];
        this.yData = [];
        this.firstVisibleDataId = 0;
        this.lastVisibleDataId  = 0;
        this.incrementation     = 1;
        this.color = {
            red     : 0,
            green   : 0,
            blue    : 0
        }
    }

    Curve.prototype = Object.create(Widget.prototype);

    Curve.prototype.add = function (xValue, yValue) {
        length = this.xData.length;
        if (length == 0 || xValue >= this.xData[length - 1]) {
            this.xData.push(xValue);
            this.yData.push(yValue);
            this.trigger("newData", this, xValue, yValue);
        }
    }

    /**
     * Display the curve.
     *
     * @this {Curve}
     * @param {Context} context The context in which to draw the widget.
     */
    Curve.prototype.display = function (context, orthonormalLandmark) {
        this.checkVisibleDataId(orthonormalLandmark);

        if (this.firstVisibleDataId < this.lastVisibleDataId) {
            context.lineWidth = 1;
            context.strokeStyle = "rgb(" + this.color.red + ", " + this.color.green + ", " + this.color.blue + ")";
            context.fillStyle = "rgb(" + this.color.red + ", " + this.color.green + ", " + this.color.blue + ")";
            context.beginPath();

            var x = this.xData[this.firstVisibleDataId],
                y = this.yData[this.firstVisibleDataId],
                relativePosX = orthonormalLandmark.getRelativePosX(x),
                relativePosY = orthonormalLandmark.getRelativePosY(y);

            context.moveTo(relativePosX, relativePosY);

            for (var i = this.firstVisibleDataId + 1; i <= this.lastVisibleDataId; i += this.incrementation) {// tmp BUG: si les data ne sont pas régulière, des pics disparaisse!!!!
                x = this.xData[i];
                y = this.yData[i];
                relativePosX = orthonormalLandmark.getRelativePosX(x);
                relativePosY = orthonormalLandmark.getRelativePosY(y);
                context.lineTo(relativePosX, relativePosY);
            }

            context.stroke();

            //tmp dot !
            if (this.incrementation == 1) {
                var size = (orthonormalLandmark.width / orthonormalLandmark.rangeX) / 3;
                size = (size > 10) ? 10 : size;
                if (size > 2) {
                    for (var i = this.firstVisibleDataId + 1; i <= this.lastVisibleDataId; i += this.incrementation) {// tmp BUG: si les data ne sont pas régulière, des pics disparaisse!!!!
                        x = this.xData[i];
                        y = this.yData[i];
                        relativePosX = orthonormalLandmark.getRelativePosX(x) - (size / 2);
                        relativePosY = orthonormalLandmark.getRelativePosY(y) - (size / 2);
                        context.fillRect(relativePosX, relativePosY, size, size);
                    }
                }
            }
        }
    };

    Curve.prototype.getYRangeDisplayedData = function (orthonormalLandmark) {//tmp name???
        var return_value = {};

        this.checkVisibleDataId(orthonormalLandmark);
        if (this.firstVisibleDataId != this.lastVisibleDataId) {
            return_value.min = this.yData[this.firstVisibleDataId];
            return_value.max = this.yData[this.firstVisibleDataId];

            for (var i = this.firstVisibleDataId + 1; i <= this.lastVisibleDataId; ++i) {//tmp parcour everything??
                return_value.min =  (this.yData[i] < return_value.min) ? this.yData[i] : return_value.min;
                return_value.max =  (this.yData[i] > return_value.max) ? this.yData[i] : return_value.max;
            }

            return return_value;
        }

        return undefined;
    }

    Curve.prototype.checkVisibleDataId = function (orthonormalLandmark) {
        var i = 0;
        if (this.xData[this.firstVisibleDataId] > orthonormalLandmark.posX) {
            while (this.xData[this.firstVisibleDataId] > orthonormalLandmark.posX && this.firstVisibleDataId != 0) {
                --this.firstVisibleDataId;
            }
        } else if (this.xData[this.firstVisibleDataId] < orthonormalLandmark.posX) {
            while (this.xData[this.firstVisibleDataId + 1] < orthonormalLandmark.posX) {
                ++this.firstVisibleDataId;
            }
        }

        if (this.lastVisibleDataId < this.firstVisibleDataId) {
            this.lastVisibleDataId = this.firstVisibleDataId;
        }

        if (this.xData[this.lastVisibleDataId] < orthonormalLandmark.posX + orthonormalLandmark.rangeX) {
            while (this.xData[this.lastVisibleDataId] < orthonormalLandmark.posX + orthonormalLandmark.rangeX && this.lastVisibleDataId < this.xData.length - 1) {
                this.lastVisibleDataId++;
            }
        }

        if (this.xData[this.lastVisibleDataId] > orthonormalLandmark.posX + orthonormalLandmark.rangeX) {
            while (this.xData[this.lastVisibleDataId - 1] > orthonormalLandmark.posX + orthonormalLandmark.rangeX) {
                this.lastVisibleDataId--;
            }
        }

        this.incrementation = 1;

        var visibleDataNumber = this.lastVisibleDataId - this.firstVisibleDataId;

        while(visibleDataNumber / this.incrementation > orthonormalLandmark.width) {
            this.incrementation++;
        }

        while (this.firstVisibleDataId % this.incrementation != 0) {
            this.firstVisibleDataId--;
        }
    };

    Curve.prototype.isInWidget = function (coordinate) {
        return false;
    };

    Curve.prototype.init = function (data) {

    };

    Curve.prototype.onMouseDown = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };

    Curve.prototype.onMouseMove = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };

    Curve.prototype.onMouseUp = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };


    Curve.prototype.onMouseWheel = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };

    Curve.prototype.setColor = function (red, green, blue) {
        this.color.red      = red;
        this.color.green    = green;
        this.color.blue     = blue;
    };

    return Curve;
});
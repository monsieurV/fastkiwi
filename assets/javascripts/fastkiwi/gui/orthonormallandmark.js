/*global window, requirejs, require, define*/

define(['fastkiwi/gui/widget', 'fastkiwi/gui/curve'], function (Widget, Curve) {
    'use strict';

    /**
     * An orthonormal landmark // tmp ?? to rename
     */
    function OrthonormalLandmark(x, y, zindex, width, height) {
        Widget.apply(this, [x, y, zindex, width, height]);

        this.posX           = -100;
        this.posY           = -5;
        this.rangeX         = 200;
        this.rangeY         = 100;
        this.axes           = [];
        this.curves         = [];//tmp ? name
        this.mouseIsDown    = false;
        this.clickMouseCoordinate;
        this.isMovable      = true;

        this.backgroundColor;
        this.AxesColor;
        this.GridColor;
        this.showAxes           = true;
        this.showGrid           = true;
        this.showScales         = true;
        this.autoScaleY         = true;
        this.autoMoveOnNewData  = false;

        this.TmpCurve = new Curve(0, 0, zindex, width, height, this);
    }

    OrthonormalLandmark.prototype = Object.create(Widget.prototype);

    OrthonormalLandmark.prototype.addCurve = function (curve) {
        this.curves.push(curve);
        curve.addEventListener("newData", this.onNewDataOnCurve, this);

        if (this.autoScaleY) {
            this.scaleY();//tmp
        }

        this.trigger("state_changed");
    };

    OrthonormalLandmark.prototype.onNewDataOnCurve = function (curve, xValue, yValue) {
         if (this.autoMoveOnNewData) {
            this.posX = xValue - this.rangeX;
            this.scaleY();//tmp ??
        }

        this.trigger("state_changed");
    };

    OrthonormalLandmark.prototype.scaleY = function () {
        if (this.curves.length > 0) {
            var range = {};
            var rangeOk = false;
            for (var i in this.curves) {
                var new_range = this.curves[i].getYRangeDisplayedData(this);
                if (new_range != undefined) {
                    if (rangeOk) {
                        range.min =  (new_range.min < range.min) ? new_range.min : range.min;
                        range.max =  (new_range.max > range.max) ? new_range.max : range.max;
                    } else {
                        range = new_range;
                    }

                    rangeOk = true;
                }
            }

            if (rangeOk) {
                var rangeSize = (range.max - range.min);
                var marge = rangeSize * 0.1;

                this.rangeY = rangeSize + marge;
                this.posY = range.min - marge / 2;

                this.trigger("state_changed");
            }
        }
    };

    /**
     * Display the orthonormal landmark.
     *
     * @this {OrthonormalLandmark}
     * @param {Context} context The context in which to draw the widget.
     */
    OrthonormalLandmark.prototype.display = function (context) {
        //console.log(this.posX + " " + this.rangeX);
        var canvas = document.createElement('canvas');
        canvas.height = this.height;
        canvas.width = this.width;
        var temp_context = canvas.getContext("2d");

        temp_context.fillStyle  = "rgb(253, 253, 253)";
        temp_context.fillRect(0, 0, this.width, this.height);
        temp_context.fill();
        
        if (this.showGrid) {
            this.displayGrid(temp_context);
        }

        if (this.showAxes) {
            this.displayAxes(temp_context);
        }

        if (this.showScales) {
            this.displayScales(temp_context);
        }

        for(var i in this.curves) {
            this.curves[i].display(temp_context, this);
        }

        //this.TmpCurve.display(temp_context);

        context.drawImage(temp_context.canvas, this.x, this.y);
    };

    OrthonormalLandmark.prototype.displayAxes = function (context) {
        context.lineWidth = 1;
        context.strokeStyle = "rgb(64, 64, 64)";

        context.beginPath();

        if (this.posX < 0 && this.posX + this.rangeX > 0) {
            var unitY   = this.getUnitY(),
                yMax    = Math.round((this.posY + this.rangeY) / unitY);

            var realPosX = this.getRelativePosX(0);
            context.moveTo(realPosX, 0);
            context.lineTo(realPosX, this.height);

            for (var y = Math.round(this.posY / unitY) + 1; y < yMax; ++y) {
                var lenght;
                if (y % 10 == 0) {
                    lenght = 10;
                } else if (y % 5 == 0) {
                    lenght = 5;
                } else {
                    lenght = 2;
                }
                
                var realPosY = this.getRelativePosY(y * unitY);
                context.moveTo(realPosX, realPosY);
                context.lineTo(realPosX + lenght, realPosY);
            }
        }

        if (this.posY < 0 && this.posY + this.rangeY > 0) {
            var unitX   = this.getUnitX(),
                xMax    = Math.round((this.posX + this.rangeX) / unitX);

            var realPosY = this.getRelativePosY(0);
            context.moveTo(0, realPosY);
            context.lineTo(0 + this.width, realPosY);

            for (var x = Math.round(this.posX / unitX) + 1; x < xMax; ++x) {
                var lenght;
                if (x % 10 == 0) {
                    lenght = 10;
                } else if (x % 5 == 0) {
                    lenght = 5;
                } else {
                    lenght = 2;
                }

                
                var realPosX = this.getRelativePosX(x * unitX);
                context.moveTo(realPosX, realPosY - lenght);
                context.lineTo(realPosX, realPosY);
            }
        }

        context.stroke();
    };

    OrthonormalLandmark.prototype.displayGrid = function (context) {
        context.lineWidth = 1;

        var unitX   = this.getUnitX(),
            xMax    = Math.round((this.posX + this.rangeX) / unitX),
            unitY   = this.getUnitY(),
            yMax    = Math.round((this.posY + this.rangeY) / unitY);


        context.strokeStyle = "rgb(230, 230, 230)";
        context.beginPath();

        for (var x = parseInt(this.posX / unitX) + 1; x < xMax; ++x) {
            var realPosX = this.getRelativePosX(x * unitX);
            context.moveTo(realPosX, 0);
            context.lineTo(realPosX, this.height);
        }

        
        for (var y = parseInt(this.posY / unitY) + 1; y < yMax; ++y) {//TMP GETuNIT
            var realPosY = this.getRelativePosY(y * unitY);
            context.moveTo(0, realPosY);
            context.lineTo(this.width, realPosY);
        }

        context.stroke();

        var x = Math.round(this.posX / unitX) + 1;
        var y = Math.round(this.posY / unitY) + 1;

        while (++x % 5 !=0 || x % 10 == 0);
        while (++y % 5 !=0 || y % 10 == 0);

        context.strokeStyle = "rgb(220, 220, 220)";
        context.beginPath();

        for (; x < xMax; x += 10) {
            var realPosX = this.getRelativePosX(x * unitX);
            context.moveTo(realPosX, 0);
            context.lineTo(realPosX, this.height);
        }

        
        for (; y < yMax; y += 10) {//TMP GETuNIT
            var realPosY = this.getRelativePosY(y * unitY);
            context.moveTo(0, realPosY);
            context.lineTo(this.width, realPosY);
        }

        context.stroke();

        var x = Math.round(this.posX / unitX) + 1;
        var y = Math.round(this.posY / unitY) + 1;

        while (++x % 10 !=0);
        while (++y % 10 !=0);

        context.strokeStyle = "rgb(180, 180, 180)";
        context.beginPath();

        for (; x < xMax; x += 10) {
            var realPosX = this.getRelativePosX(x * unitX);
            context.moveTo(realPosX, 0);
            context.lineTo(realPosX, this.height);
        }

        
        for (; y < yMax; y += 10) {//TMP GETuNIT
            var realPosY = this.getRelativePosY(y * unitY);
            context.moveTo(0, realPosY);
            context.lineTo(this.width, realPosY);
        }

        context.stroke();
    };

    OrthonormalLandmark.prototype.displayScales = function (context) {
        var unitX   = this.getUnitX(),
            xMax    = Math.round((this.posX + this.rangeX) / unitX),
            unitY   = this.getUnitY(),
            yMax    = Math.round((this.posY + this.rangeY) / unitY);

        context.lineWidth = 1;
        context.strokeStyle = "rgb(254, 64, 64)";

        context.beginPath();

        context.moveTo(0, this.height);
        context.lineTo(this.width, this.height);

        context.moveTo(0, 0);
        context.lineTo(this.width, 0);

        for (var x = Math.round(this.posX / unitX) + 1; x < xMax; ++x) {
            var lenght;
            if (x % 10 == 0) {
                lenght = 10;
            } else if (x % 5 == 0) {
                lenght = 5;
            } else {
                lenght = 2;
            }

            
            var realPosX = this.getRelativePosX(x * unitX);
            context.moveTo(realPosX, this.height - lenght);
            context.lineTo(realPosX, this.height);

            context.moveTo(realPosX, 0);
            context.lineTo(realPosX, lenght);
        }

        context.moveTo(0, 0);
        context.lineTo(0, this.height);

        context.moveTo(this.width, 0);
        context.lineTo(this.width, this.height);

        for (var y = Math.round(this.posY / unitY) + 1; y < yMax; ++y) {
            var lenght;
            if (y % 10 == 0) {
                lenght = 10;
            } else if (y % 5 == 0) {
                lenght = 5;
            } else {
                lenght = 2;
            }
            
            var realPosY = this.getRelativePosY(y * unitY);
            context.moveTo(0, realPosY);
            context.lineTo(lenght, realPosY);

            context.moveTo(this.width - lenght, realPosY);
            context.lineTo(this.width, realPosY);
        }

        context.stroke();
    };

    OrthonormalLandmark.prototype.getUnitX = function () {
        var unitX = 1;

        while (this.width / (this.rangeX / unitX) < 15) {
            unitX *= 5;
        }

        while (this.width / (this.rangeX / unitX) > 25) {
            unitX *= 0.2;
        }

        return unitX;
    }

    OrthonormalLandmark.prototype.getUnitY = function () {
        var unitY = 1;
        
        while (this.height / (this.rangeY / unitY) < 10) {
            unitY *= 5;
        }

        while (this.height / (this.rangeY / unitY) > 25) {
            unitY *= 0.2;
        }

        return unitY;
    }

    OrthonormalLandmark.prototype.getAbsolutePosX = function (gridPosX) {
        return this.x + this.getRelativePosX(gridPosX);
    }

    OrthonormalLandmark.prototype.getAbsolutePosY = function (gridPosY) {
        return this.y + this.getRelativePosY(gridPosY);
    }

    OrthonormalLandmark.prototype.getRelativePosX = function (gridPosX) {
        return (gridPosX - this.posX) * (this.width / this.rangeX);
    }

    OrthonormalLandmark.prototype.getRelativePosY = function (gridPosY) {
        return this.height - (gridPosY - this.posY) * (this.height / this.rangeY);
    }

    OrthonormalLandmark.prototype.getGridPosX = function (realPosX) {
        return (this.posX + (realPosX - 0) / (this.width / this.rangeX));
    }

    OrthonormalLandmark.prototype.getGridPosY = function (realPosY) {
        return (this.posY + (realPosY - 0) / (this.height / this.rangeY));
    }

    OrthonormalLandmark.prototype.init = function (data) {

    };

    OrthonormalLandmark.prototype.onMouseDown = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            this.mouseIsDown = true;
            this.clickMouseCoordinate = mouseCoordinate; 

            if(this.isMovable) {
                document.body.style.cursor = "move";//"ns-resize"
            }

            return false;
        }

        return true;
    };

    OrthonormalLandmark.prototype.onMouseMove = function (e, propageEvent, mouseCoordinate) {
        if (this.mouseIsDown && this.isMovable) {
            this.posX -= (mouseCoordinate.x - this.clickMouseCoordinate.x) / (this.width / this.rangeX);
            this.posY += (mouseCoordinate.y - this.clickMouseCoordinate.y) / (this.height / this.rangeY);
            this.clickMouseCoordinate = mouseCoordinate;
            if (this.autoScaleY) {
                this.scaleY();//tmp
            }
            this.trigger("state_changed");
        }

        if (this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };

    OrthonormalLandmark.prototype.onMouseUp = function (e, propageEvent, mouseCoordinate) {
        if(this.mouseIsDown) {
            this.mouseIsDown = false;
            document.body.style.cursor = "auto";
        }

        if (this.isInWidget(mouseCoordinate)) {
            return false;
        }

        return true;
    };


    OrthonormalLandmark.prototype.onMouseWheel = function (e, propageEvent, mouseCoordinate) {
        if (propageEvent && this.isInWidget(mouseCoordinate)) {
            var realX = this.getGridPosX(mouseCoordinate.x),
                relativeX = mouseCoordinate.x - 0,
                realY = this.getGridPosY(mouseCoordinate.y);
            if (e.delta > 0) {//tmp detail is just for firefox
                this.rangeX *= 0.99;//tmp speed factor
            } else {
                this.rangeX *= 1.01;
            }

            this.posX = realX - (this.rangeX / (this.width / relativeX));
            if (this.autoScaleY) {
                this.scaleY();//tmp
            }
            this.trigger("state_changed");
            return false;
        }
        
        return true;
    };

    return OrthonormalLandmark;
});
/*global window, requirejs, require, define*/

define(['fastkiwi/eventshandler'], function (EventsHandler) {
    'use strict';

    /**
     * An abstract class representing a widget.
     *
     * @name Widget
     * @constructor
     * @augment EventsHandler
     * @this {Widget}
     * @param {Number} x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} zindex The zindex of the widget.
     * @param {Number} width The width of the widget.
     * @param {Number} height The height of the widget.
     * @property {Number} x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @property {Number} y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @property {Number} zindex The zindex of the widget.
     * @property {Number} width The width of the widget.
     * @property {Number} height The height of the widget.
     */
    function Widget(x, y, zindex, width, height) {
        EventsHandler.apply(this);

        this.zindex = (typeof zindex    !== 'number') ? 0 : zindex;
        this.x      = (typeof x         !== 'number') ? 0 : x;
        this.y      = (typeof y         !== 'number') ? 0 : y;
        this.height = (typeof height    !== 'number') ? 0 : height;
        this.width  = (typeof width     !== 'number') ? 0 : width;
    }

    Widget.prototype = Object.create(EventsHandler.prototype);

    /**
     * Abstract method. Must be redefined in the childs class.
     *
     * @this {Widget}
     * @param {Context} context The context in which to draw the widget.
     */
    Widget.prototype.display = function (context) {
        return undefined;
    };

    /**
     * Init a widget.
     *
     * @this {Widget}
     * @param {Object} data The init data.
     * @param {Number} data.x The x-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} data.y The y-coordinate of the widget relative to the top left corner of the Parent Element.
     * @param {Number} data.zindex The zindex of the widget.
     * @param {Number} data.width The width of the widget.
     * @param {Number} data.height The height of the widget.
     */
    Widget.prototype.init = function (data) {
        this.x      = (typeof data.x         !== 'number') ? this.x         : data.x;
        this.y      = (typeof data.y         !== 'number') ? this.y         : data.y;
        this.zindex = (typeof data.zindex    !== 'number') ? this.zindex    : data.zindex;
        this.height = (typeof data.height    !== 'number') ? this.height    : data.height;
        this.width  = (typeof data.width     !== 'number') ? this.width     : data.width;
    };

    /**
     * Is a coordinate in the widget.
     *
     * @this {Widget}
     * @param {Object} coordinate The coordinate to check relative to the top left corner of the Parent Element.
     * @param {Number} coordinate.x The x-coordinate.
     * @param {Number} coordinate.y The y-coordinate.
     * @return {Boolean} True if the coordinate is in the widget.
     */
    Widget.prototype.isInWidget = function (coordinate) {
        return (coordinate.x >= this.x && coordinate.x <= this.x + this.width &&
                coordinate.y >= this.y && coordinate.y <= this.y + this.height);
    };

    /**
     * The default widget action when a key is down.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onKeyDown = function (e, propageEvent) {
        return propageEvent;
    };

    /**
     * The default widget action when a key is up.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onKeyUp = function (e, propageEvent) {
        return propageEvent;
    };

    /**
     * The default widget action when a mouse button is down.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onMouseDown = function (e, propageEvent, mouseCoordinate) {
        return !this.isInWidget(mouseCoordinate) && propageEvent;
    };

    /**
     * The default widget action when the mouse is mouved.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onMouseMove = function (e, propageEvent, mouseCoordinate) {
        return !this.isInWidget(mouseCoordinate) && propageEvent;
    };

    /**
     * The default widget action when a mouse button is up.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onMouseUp = function (e, propageEvent, mouseCoordinate) {
        return !this.isInWidget(mouseCoordinate) && propageEvent;
    };

    /**
     * The default widget action when the wheel of the mouse scrolled.
     *
     * @this {Widget}
     * @param {Event} e The event.
     * @param {Boolean} propageEvent False if the event is allready blocked by another widget. True else.
     * @param {Object} mouseCoordinate The mouse coordinate of the event relative to the top left corner of the Parent Element.
     * @param {Number} mouseCoordinate.x The x-coordinate.
     * @param {Number} mouseCoordinate.y The y-coordinate.
     * @return {Boolean} False if the event is blocked by the widget or alleready blocked. True else.
     */
    Widget.prototype.onMouseWheel = function (e, propageEvent, mouseCoordinate) {
        return !this.isInWidget(mouseCoordinate) && propageEvent;
    };

    Widget.prototype.onScaleChanged = function(scale) {
        this.x      = Math.round(this.x * scale);
        this.y      = Math.round(this.y * scale);
        this.width  = Math.round(this.width * scale);
        this.height = Math.round(this.height * scale);
    };

    return Widget;
});
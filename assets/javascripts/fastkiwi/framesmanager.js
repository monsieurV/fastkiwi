/*global window, requirejs, require, define*/

define(['fastkiwi/multipleelementsmanager'], function (MultipleElementsManager) {
    'use strict';

    /**
     * Manage the frames and the framesListeners.
     *
     * @name FramesManager
     * @constructor
     * @augments MultipleElementsManager
     * @this {FramesManager}
     * @param {Gui} gui A reference to the gui of the graphic engine.
     * @property {Number} startTime The start rendering time.
     * @property {Number} timeSinceLastFrame The time between the beginning of the previous frame and the beginning of the current frame.
     * @property {Number} lastFrameStartTime The beginning time of the previous frame.
     * @property {Number} fps The current number of frames per second (FPS).
     * @property {Number} lastFpsCalculation The time between the last fps calculation and now.
     * @property {Array} fpsBuffer A buffer containing the 25 previous frame rendering time.
     * @property {Boolean} isRendering Contain true if the graphic engine is rendering
     * @property {Boolean} renderingRequest Contain the last rendering request. True if this is a start request, False else.
     * @property {Gui} gui A reference to the gui of the graphic engine.
     * @property {Array} elements An associative array containing all frames listeners.<br/><br/>
     *      A frames Listener is a literal object that contain some attributes : <br/>
     *      - 'frameStarted': The method to execute every frame.
                This method take one parameter : the time since the last execution of this function.<br/>
     *      - 'scope' : The function scope.
     */
    function FramesManager(gui) {
        MultipleElementsManager.apply(this);

        this.startTime              = 0;
        this.timeSinceLastFrame     = 0;
        this.lastFrameStartTime     = 0;
        this.fps                    = 0;
        this.lastFpsCalculation     = 0;
        this.fpsBuffer              = [];
        this.isRendering            = false;
        this.renderingRequest       = false;
        this.gui                    = (typeof gui === 'object') ? gui : null;
    }

    FramesManager.prototype = Object.create(MultipleElementsManager.prototype);

    /**
    * Calculate the number of frames per second (FPS). 
    * This calculate is effectuate maximum one time per 500 ms by calculating the average of the rendering time of the 25 last frames.
    * It's must be called every frame. This is automatically done if the calculateFps method is called.
    *
    * @this {FramesManager}
    */
    FramesManager.prototype.calculateFps = function () {
        if (this.timeSinceLastFrame > 0) {
            this.fpsBuffer.push(1000 / this.timeSinceLastFrame);
        }

        if (this.fpsBuffer.length > 25) {
            this.fpsBuffer.shift();

            if (new Date().getTime() - this.lastFpsCalculation > 500) {
                var fpsSomme = 0,
                    bufferSize = this.fpsBuffer.length,
                    i;

                this.lastFpsCalculation = new Date().getTime();

                for (i = 0; i < bufferSize; i += 1) {
                    fpsSomme += this.fpsBuffer[i];
                }

                this.fps = Math.round(fpsSomme / bufferSize);
            }
        }
    };

    /**
    * Enable / Disable the fps calculation.
    *
    * @this {FramesManager}
    * @param {Boolean} [enable=true] Enable the fpsCalculation if its set to true.
    */
    FramesManager.prototype.enableFpsCalculation = function (enable) {
        enable = (enable === undefined) ? true : enable;

        if (enable) {
            var fpsListener = {
                'frameStarted'  : this.calculateFps,
                'scope'         : this
            };

            this.add(fpsListener, '__fpsListener');
        } else {
            this.remove('__fpsListener');
        }
    };

    /**
    * Call the renderOneFrame method about 60 times per second.
    *
    * @private
    * @this {FramesManager}
    */
    FramesManager.prototype.rendering = function () {
        var self = this;

        this.isRendering = this.renderingRequest;

        if (this.isRendering) {
            window.requestAnimFrame(function () {
                self.renderOneFrame();
                self.rendering();
            });
        }
    };

    /**
    * Render a frame. Calculate fps, call the framesListeners and display functions.
    *
    * @this {FramesManager}
    */
    FramesManager.prototype.renderOneFrame = function () {
        var i;

        this.timeSinceLastFrame = new Date().getTime() - this.lastFrameStartTime;
        this.lastFrameStartTime = new Date().getTime();

        for (i in this.elements) {
            if (this.elements.hasOwnProperty(i)) {
                this.elements[i].frameStarted.call(this.elements[i].scope, this.timeSinceLastFrame);
            }
        }

        this.gui.display();
    };

    /**
    * Start / Stop the rendering and initialize time.
    *
    * @this {FramesManager}
    * @param {Boolean} [start=true] Start the rendering if its set to true.
    */
    FramesManager.prototype.startRendering = function (start) {
        start = (typeof start !== 'boolean') ? true : start;
        this.renderingRequest = start;

        if (start && !this.isRendering) {
            this.startTime          = new Date().getTime();
            this.lastFrameStartTime = new Date().getTime();
            this.frameStartTime     = new Date().getTime();
            this.lastFpsCalculation = new Date().getTime();
            this.timeSinceLastFrame = 0;
            this.startTime          = 0;
            this.fps                = 0;
            this.fpsBuffer          = [];

            this.rendering();
        }
    };

    return FramesManager;
});

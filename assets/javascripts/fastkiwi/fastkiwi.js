/*global window, requirejs, require, define*/

define(function () {
    'use strict';

    /**
     * Represent the graphic engine.
     *
     * @name Fastkiwi
     * @constructor
     * @this {Fastkiwi}
     * @property {Canvas} canvas The canvas used for the graphic engine.
     * @property {CanvasRenderingContext2D} context The 2d canvas context.
     */
    function Fastkiwi(canvas) {
        this.canvas     = canvas;
        this.context    = (this.canvas === undefined) ? undefined : canvas.getContext('2d');
    }

    return Fastkiwi;
});

/*

    this.addInputListener = function(inputListeners) {
    };

    this.removeInputListener = function(inputListeners) {
    };

    this.addWidget();

*/

/*global window, requirejs, require, define*/

define(['fastkiwi/multipleelementsmanager'], function (MultipleElementsManager) {
    'use strict';

    /**
     * Manage and propaged user events.
     *
     * @name InputsManager
     * @constructor
     * @augments MultipleElementsManager
     * @this {InputsManager}
     * @param {Canvas} canvas The canvas used by the graphic engine.
     * @param {ScreenManager} screenManager The screen manager used by the graphic engine.
     * @param {Gui} gui The gui used by the graphic engine.
     * @property {Array} keysState An array containing all current keys states (true if down, false if up).
     * @property {Canvas} canvas The canvas used by the graphic engine.
     * @property {ScreenManager} screenManager The screen manager used by the graphic engine.
     * @property {Gui} gui The gui used by the graphic engine.
     */
    function InputsManager(canvas, screenManager, gui) {
        MultipleElementsManager.apply(this);
        this.keysState  = [];
        this.canvas                 = canvas;
        this.screenManager          = screenManager;
        this.gui                    = gui;
    }

    InputsManager.prototype = Object.create(MultipleElementsManager.prototype);

    /**
    * Get the event mouse coordinate relative to the top left corner of the canvas. 
    *
    * @this {InputsManager}
    * @param {Event} e The current event object.
    */
    InputsManager.prototype.getCanvasMouseCoordinate = function (e) {
        var canvasOriginX,
            canvasOriginY,
            boxObject,
            rect,
            canvasMouseCoordinate;

        if (window.document.getBoxObjectFor) {
            boxObject = window.document.getBoxObjectFor(this.canvas);
            canvasOriginX = boxObject.x;
            canvasOriginY = boxObject.y;
        } else {
            rect = this.canvas.getBoundingClientRect();
            canvasOriginX = rect.left;
            canvasOriginY = rect.top;
        }

        canvasMouseCoordinate = {
            'x' : e.clientX - canvasOriginX,
            'y' : e.clientY - canvasOriginY
        };

        canvasMouseCoordinate.x -= this.screenManager.canvasShiftX;
        canvasMouseCoordinate.y -= this.screenManager.canvasShiftY;

        return canvasMouseCoordinate;
    };

    /**
    * Init the inputs manager. Must be called before using inputs manager functionality.
    *
    * @this {InputsManager}
    */
    InputsManager.prototype.init = function () {
        var self = this,
            i;

        for (i = 0; i < 223; i += 1) {
            this.keysState[i] = false;
        }

        window.window.document.addEventListener('keydown',          function (e) { if (!self.intercept(e, 'Key', 'Down')) { e.preventDefault(); } });
        window.window.document.addEventListener('keyup',            function (e) { if (!self.intercept(e, 'Key', 'Up')) { e.preventDefault(); } });
        window.window.document.addEventListener('mousedown',        function (e) { if (!self.intercept(e, 'Mouse', 'Down')) { e.preventDefault(); } });
        window.window.document.addEventListener('mouseup',          function (e) { if (!self.intercept(e, 'Mouse', 'Up')) { e.preventDefault(); } });
        window.window.document.addEventListener('mousemove',        function (e) { if (!self.intercept(e, 'Mouse', 'Move')) { e.preventDefault(); } });
        window.window.document.addEventListener('mousewheel',       function (e) { if (!self.intercept(e, 'Mouse', 'Wheel')) { e.preventDefault(); } });
        window.window.document.addEventListener("DOMMouseScroll",   function (e) { if (!self.intercept(e, 'Mouse', 'Wheel')) { e.preventDefault(); } });
        window.window.document.addEventListener('contextmenu',      function (e) {
            if (self.isInCanvas(self.getCanvasMouseCoordinate(e))) {
                e.preventDefault();
            }
        });
    };

    /**
    * intercept user inputs events. 
    *
    * @this {InputsManager}
    * @param {Event} e the current event object.
    * @param {String} device The name of the device causing the event ('Mouse' or 'Key').
    * @param {String} type The name of the curent event type ('Up', 'Down', 'Move' or 'Wheel').
    */
    InputsManager.prototype.intercept = function (e, device, type) {
        var canvasMouseCoordinate,
            propageEvent = true;

        switch (device) {
        case 'Key':
            switch (type) {
            case 'Down':
                if (this.keysState[e.keyCode]) {
                    return;
                }

                this.keysState[e.keyCode] = true;
                break;
            case 'Up':
                this.keysState[e.keyCode] = false;
                break;
            }

            break;
        case 'Mouse':
            canvasMouseCoordinate   = this.getCanvasMouseCoordinate(e);

            switch (type) {
            case 'Wheel':
                e.delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
            }

            break;
        }

        return this.propage(e, device, type, propageEvent, canvasMouseCoordinate);
    };

    /**
    * Is a coodinate in canvas?
    *
    * @this {InputsManager}
    * @param {Object} coordinate A literal objet contening the coodinate relative to the top left corner of the canvas.
    * @param {Number} coordinate.x The x-coordinate.
    * @param {Number} coordinate.y The y-coordinate.
    * @return {Boolean} True if the coodinate is situate in the canvas.
    */
    InputsManager.prototype.isInCanvas = function (coordinate) {
        return (coordinate.x >= 0 && coordinate.x <= this.gui.width &&
                coordinate.y >= 0 && coordinate.y <= this.gui.height);
    };

    /**
    * propage user inputs events. 
    *
    * @this {InputsManager}
    * @param {Event} e the current event object.
    * @param {String} device The name of the device causing the event ('Mouse' or 'Key').
    * @param {String} type The name of the curent event type ('Up', 'Down', 'Move' or 'Wheel').
    * @param {Boolean} propageEvent The event must be propaged or just considered.
    * @param {Object} canvasMouseCoordinate A literal objet contening the mouse coodinate relative to the top left corner of the canvas.
    * @param {Number} canvasMouseCoordinate.x The x-coordinate of the mouse.
    * @param {Number} canvasMouseCoordinate.y The y-coordinate of the mouse.
    */
    InputsManager.prototype.propage = function (e, device, type, propageEvent, canvasMouseCoordinate) {
        var i;

        if (this.gui) {
            propageEvent = this.gui.propage(e, device, type, propageEvent, canvasMouseCoordinate)
        }

        if (propageEvent) { //tmp ?
            for (i in this.elements) {
                if (this.elements.hasOwnProperty(i)) {
                    propageEvent = this.elements[i]['on' + device + type](e, canvasMouseCoordinate);
                }
            }
        }

        return propageEvent;
    };

    return InputsManager;
});
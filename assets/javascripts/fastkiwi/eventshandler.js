/*global window, requirejs, require, define*/

define(function () {
    'use strict';

    /**
     * Manager for multiple element.
     *
     * @name EventsHandler
     * @constructor
     * @this {EventsHandler}
     * @property {Array} eventListeners An array contening all listener shorted by the event type.
     * @property {Array} scopes An array contening all scopes shorted by the event type.
     */
    function EventsHandler() {
        this.eventListeners = [];
        this.scopes         = [];
    }

    /**
    * Add an event listener.
    *
    * @this {EventsHandler}
    * @param {String} type The name of the event.
    * @param {Function} listener The action to realize when the event is triggered.
    * @param {Object} [scope=null] The scope of the listener.
    */
    EventsHandler.prototype.addEventListener = function (type, listener, scope) {
        if (typeof type === 'string' && typeof listener === 'function') {
            scope = (typeof scope !== 'object') ? null : scope;
            this.eventListeners[type] = (this.eventListeners[type] === undefined) ? [] : this.eventListeners[type];
            this.scopes[type] = (this.scopes[type] === undefined) ? [] : this.scopes[type];
            this.eventListeners[type].push(listener);
            this.scopes[type].push(scope);
        }
    };

    /**
    * Remove en event listener.
    *
    * @this {EventsHandler}
    * @param {String} type The type of the event.
    * @param {Function} listener The listener to remove.
    * @param {Object} [scope=null] The scope of the listener. If not set, the removal don't care about the scope.
    * @param {Number} [quantity=1] The number of listener to remove.
    *   By default, only the last added is deleted.
    *   If ist a negative number, then all corresponding listeners will be removed.
    * @param {Number} [startIndex=last listener index] The index to start the research of the listener.
    *   This parameter must be used only by the recursion of the function!
    */
    EventsHandler.prototype.removeEventListener = function (type, listener, scope, quantity, startIndex) {
        if (typeof type === 'string' && this.eventListeners[type] !== undefined &&
                typeof listener === 'function') {
            quantity = (typeof quantity !== 'number') ? 1 : quantity;

            if (quantity !== 0) {
                startIndex = (typeof startIndex !== 'number' || startIndex < 0) ? this.eventListeners[type].length : startIndex;
                var index = this.eventListeners[type].lastIndexOf(listener, startIndex);

                if (index >= 0) {
                    if (typeof scope !== 'object' || scope === null ||
                            (typeof scope === 'object' && this.scopes[type][index] === scope)) {
                        this.eventListeners[type].splice(index, 1);
                        this.scopes[type].splice(index, 1);
                        quantity -= 1;
                    }

                    this.removeEventListener(type, listener, scope, quantity, index - 1);
                }
            }
        }
    };

    /**
    * Trigger an event. Call the listeners of an event.
    *
    * @this {EventsHandler}
    * @param {String} type The type of the event.
    */
    EventsHandler.prototype.trigger = function (type) {
        if (typeof type === 'string' && this.eventListeners[type] !== undefined) {
            var listeners = this.eventListeners[type],
                scopes = this.scopes[type],
                args = Array.prototype.slice.call(arguments),
                length = listeners.length,
                i;

            args.shift();

            for (i = 0; i < length; i += 1) {
                listeners[i].apply(scopes[i], args);
            }
        }
    };

    return EventsHandler;
});

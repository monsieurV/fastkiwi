/*global window, requirejs, require, define*/

define(['fastkiwi/eventshandler'], function (EventsHandler) {
    'use strict';

    /**
     * Manage canvas size and fullscreen.
     *
     * @name ScreenManager
     * @constructor
     * @this {ScreenManager}
     * @param {Context} context The context used by the graphic engine.
     * @property {Context} context The context used by the graphic engine.
     * @property {Canvas} canvas The canvas used by the graphic engine.
     * @property {Number} canvasHeight The height of the canvas.
     * @property {Nomber} canvasWidth The width of the canvas
     * @property {Boolean} isInFullSreen True if the canvas is in full screen mode.
     * @property {Boolean} isInFullDocumment True if the canvas is in full document mode.
     * @property {Function} onWindowResized A function used by the full document mode when the window is resized.
     * @property {Number} canvasScale The scale of the canvas.
     * @property {Number} canvasShiftX The shift in the x-axis. Its useful when canvas is in full screen/document mode.
     * @property {Number} canvasShiftY The shift in the y-axis. Its useful when canvas is in full screen/document mode.
     */
    function ScreenManager(context) {
        EventsHandler.apply(this);

        this.context            = context;
        this.canvas             = context.canvas;
        this.canvasHeight       = this.canvas.height;
        this.canvasWidth        = this.canvas.width;
        this.isInFullSreen      = false;
        this.isInFullDocumment  = false;
        this.onWindowResized    = undefined;
        this.canvasScale        = 1;
        this.canvasShiftX       = 0;
        this.canvasShiftY       = 0;
    }

    ScreenManager.prototype = Object.create(EventsHandler.prototype);

    /**
    * Adapt the canvas size when starting / stoping fullscreen or fulldocument mode. 
    *
    * @this {ScreenManager}
    * @param {Boolean} fullMod True if starting a full mod, false if canceling a fullmod.
    * @param {Number} height The height of the full mode. Useles if fullMod is false.
    * @param {Number} width The width of the full mode.  Useles if fullMod is false.
    */
    ScreenManager.prototype.adaptCanvas = function (fullMod, height, width) {
        if (fullMod) {
            if (this.canvasHeight / height > this.canvasWidth / width) {
                this.canvasScale  = height / this.canvasHeight;
                this.canvasShiftX =  Math.round((width - (this.canvasWidth * this.canvasScale)) / 2);
            } else {
                this.canvasScale    = width / this.canvasWidth;
                this.canvasShiftY   = Math.round((height - (this.canvasHeight * this.canvasScale)) / 2);
            }

            this.canvas.height          = height;
            this.canvas.width           = width;
            this.canvas.style.height    = height + "px";
            this.canvas.style.width     = width + "px";
            this.context.translate(this.canvasShiftX, this.canvasShiftY);
            this.trigger("on_scale_changed", this.canvasScale);
        } else {
            this.canvas.height          = this.canvasHeight;
            this.canvas.width           = this.canvasWidth;
            this.canvas.style.height    = this.canvasHeight + "px";
            this.canvas.style.width     = this.canvasWidth + "px";
            this.canvasScale            = 1;
            this.canvasShiftX           = 0;
            this.canvasShiftY           = 0;
            this.trigger("on_scale_changed", this.canvasScale);
        }
    };

    /**
    * Resize the canvas using the size of the screen, with a different scale. 
    *
    * @this {ScreenManager}
    * @param {Number} [scale=0.4] The new scale of the canvas.
    */
    ScreenManager.prototype.autoResizeCanvas = function (scale) {
        scale = (typeof scale !== 'number' || scale < 0.05 || scale > 1) ? 0.4 : scale;
        this.resizeCanvas(window.screen.width * scale, window.screen.height * scale);
    };

    /**
    * Stop the full document mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.cancelFullDocument = function () {
        if (this.isInFullDocumment) {
            this.isInFullDocumment      = false;
            window.removeEventListener('resize', this.onWindowResized);
            this.canvas.style.position  = 'relative';
            this.canvas.style.top       = '';
            this.canvas.style.left      = '';
            this.canvas.style.zindex    = '';

            this.adaptCanvas(false);
            this.trigger("full_document_toggled", false);
        }
    };

    /**
    * Stop the fullscreen mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.cancelFullScreen = function () {
        if (window.document.cancelFullScreen) {
            window.document.cancelFullScreen();
        } else if (window.document.mozCancelFullScreen) {
            window.document.mozCancelFullScreen();
        } else if (window.document.webkitCancelFullScreen) {
            window.document.webkitCancelFullScreen();
        }
    };

    /**
    * Init the screen manager. Must be called before using screen manager functionality.
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.init = function () {
        var self = this;

        if (window.document.documentElement.requestFullscreen) {
            window.document.addEventListener("fullscreenchange", function () { self.onFullScreenChange(); });
        } else if (window.document.documentElement.webkitRequestFullScreen) {
            window.document.addEventListener("webkitfullscreenchange", function () { self.onFullScreenChange(); });
        } else if (window.document.documentElement.mozRequestFullScreen) {
            window.document.addEventListener("mozfullscreenchange", function () { self.onFullScreenChange(); });
        }
    };

    /**
    * The canvas is in fullscreen mode?
    *
    * @this {ScreenManager}
    * @return {Boolean} True if the canvas is in fullscreen mode.
    */
    ScreenManager.prototype.isCanvasInFullScreen = function () {
        return ((window.document.fullScreenElement && window.document.fullScreenElement === this.canvas) ||
            (window.document.mozFullScreenElement && window.document.mozFullScreenElement === this.canvas) ||
            (window.document.webkitFullscreenElement && window.document.webkitFullscreenElement === this.canvas));
    };

    /**
    * The document is in fullscreen mode?
    *
    * @this {ScreenManager}
    * @return {Boolean} True if the document is in fullscreen mode.
    */
    ScreenManager.prototype.isDocumentInFullScreenMode = function () {
        return ((window.document.fullScreenElement && window.document.fullScreenElement !== null) ||
          (!window.document.mozFullScreen && !window.document.webkitIsFullScreen));
    };

    /**
    * Methode called when the fullscreenchange event is trigger.
    * Its adapt the canvas fonction of the screen mode. 
    *
    * @private
    * @this {ScreenManager}
    */
    ScreenManager.prototype.onFullScreenChange = function () {
        if (this.isCanvasInFullScreen()) {
            this.isInFullScreen = true;
            this.adaptCanvas(true, window.screen.height, window.screen.width);
            this.trigger("full_screen_toggled", true);
        } else {
            this.isInFullScreen = false;
            this.adaptCanvas(false);
            this.trigger("full_screen_toggled", false);
        }
    };

    /**
    * Start the full document mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.setFullDocument = function () {
        if (!this.isInFullDocumment) {
            var self                = this;
            this.isInFullDocumment  = true;

            this.adaptCanvas(true, window.innerHeight, window.innerWidth);

            this.canvas.style.position  = 'fixed';
            this.canvas.style.top       = '0px';
            this.canvas.style.left      = '0px';
            this.canvas.style.zindex    = "100";

            this.onWindowResized = function () {
                self.adaptCanvas(true, window.innerHeight, window.innerWidth);
            };

            window.addEventListener('resize', this.onWindowResized);

            this.trigger("full_document_toggled", true);
        }
    };

    /**
    * Start the fullscreen mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.setFullScreen = function () {
        if (window.document.documentElement.requestFullscreen) {
            this.canvas.requestFullscreen();
        } else if (window.document.documentElement.mozRequestFullScreen) {
            this.canvas.mozRequestFullScreen();
        } else if (window.document.documentElement.webkitRequestFullscreen) {
            this.canvas.webkitRequestFullscreen(window.Element.ALLOW_KEYBOARD_INPUT);
        }
    };

    /**
    * Resize the canvas. 
    *
    * @this {ScreenManager}
    * @param {Number} [height] The new height of the canvas.
    * @param {Number} [width] The new width of the canvas.
    */
    ScreenManager.prototype.resizeCanvas = function (width, height) {
        this.canvasWidth =  this.canvas.width = width;
        this.canvas.style.width  = this.canvas.width + "px";
        this.canvasHeight =  this.canvas.height = height;
        this.canvas.style.height  = this.canvas.height + "px";
    };

    /**
    * Toggle full document mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.toggleFullDocument = function (fullDocument) {
        fullDocument = (fullDocument === undefined) ? !this.isInFullDocumment : fullDocument;

        if (fullDocument) {
            this.setFullDocument();
        } else {
            this.cancelFullDocument();
        }
    };

    /**
    * Toggle fullscreen mode. 
    *
    * @this {ScreenManager}
    */
    ScreenManager.prototype.toggleFullScreen = function (fullScreen) {
        fullScreen = (fullScreen === undefined) ? !this.isCanvasInFullScreen() : fullScreen;

        if (fullScreen) {
            this.setFullScreen();
        } else {
            this.cancelFullScreen();
        }
    };

    return ScreenManager;
});
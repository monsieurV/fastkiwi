/*global window, requirejs, require, define*/

define(['fastkiwi/eventshandler'], function (EventsHandler) {
    'use strict';

    /**
     * Manager for multiple element.
     *
     * @name MultipleElementsManager
     * @constructor
     * @augments EventsHandler
     * @this {MultipleElementsManager}
     * @property {Array} elements An array containing all elements.
     * @property {Array} numberOfUnnamedElements The quantity of unnamed elements used.
     */
    function MultipleElementsManager() {
        EventsHandler.apply(this);

        this.elements                   = [];
        this.sortedElements             = [];
        this.numberOfUnnamedElements    = 0;
    }

    MultipleElementsManager.prototype = Object.create(EventsHandler.prototype);

    /**
    * Add an element.
    * Its trigger the "element_added" event whit the element name in parameter.
    *
    * @this {MultipleElementsManager}
    * @param {Object} element The element to add.
    * @param {String} [name="unnamed_<id>"] The element name useful to retrieve or remove it later.
    * @return {String} The name of the added element or an empty string if nothing added.
    */
    MultipleElementsManager.prototype.add = function (element, name) {
        if (typeof element === 'object') {
            if (typeof name !== 'string' || name === '' || this.elements.hasOwnProperty(name)) {
                name = 'unnamed_' + this.numberOfUnnamedElements;
                this.numberOfUnnamedElements += 1;
            }

            this.elements[name] = element;

            var property;
            for (property in this.sortedElements) {
                if (this.sortedElements.hasOwnProperty(property)) {
                    this.sortedElements[property].elements.push(this.elements[name]);
                    this.sortedElements[property].modified = true;

                    if (this.sortedElements[property].checkValueChange) {
                        this.addCheckValueOn(property, this.elements[name]);
                    }
                }
            }

            this.trigger('element_added', name);

            return name;
        }

        return '';
    };

    /**
    * Enable the check of value change on a property of a particular element.
    *
    * @private
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    * @param {String} element The element witch enable check property value.
    */
    MultipleElementsManager.prototype.addCheckValueOn = function (property, element) {
        var self = this;

        if (typeof element === 'object' && element !== null && element.hasOwnProperty(property)) {
            Object.defineProperty(element, '__' + property, Object.getOwnPropertyDescriptor(element, property));
            Object.defineProperty(element, property, {
                get: function () { return this['__' + property]; },
                set: function (newValue) {
                    self.valueChanged(property);
                    this['__' + property] = newValue;
                }
            });
        }
    };

    /**
    * Enable / Disable the sorted array caching.
    * It increase the performance of the getAllSortedBy method on the auto sorted type.
    * The sorted elements are stocked in a new simple array.
    * You can enable caching several times for several type.
    * You can't enable caching two times for the same type.
    *
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property to use to sort the elements.
    * @param {Boolean} [enable=true] If true, the caching is enabled.
    * @param {Boolean} [checkValueChange=false] If true, it enable the checking of the value modification of the property of an element.
    *   The resort is then performed only if the value of the property of an element is modified.
    *   Be careful, the property must be configurable and not defined with Constructor.prototype.
    *   Use the check property modification only if there is less property value modification or adding than getAllSortedBy method call.  
    */
    MultipleElementsManager.prototype.cacheArraySortedBy = function (property, enable, checkValueChange) {
        if (typeof property === 'string') {
            enable = (typeof enable !== 'boolean') ? true : enable;

            if (enable) {
                if (this.sortedElements[property] === undefined) {
                    this.sortedElements[property] = {
                        'checkValueChange'  : false,
                        'modified'          : false,
                        'ascending'         : true,
                        'elements'          : this.getAllSortedBy(property)
                    };
                }

                if (checkValueChange && !this.sortedElements[property].checkValueChange) {
                    this.enableCheckValueChange(property);
                } else if (!checkValueChange && this.sortedElements[property].checkValueChange) {
                    this.disableCheckValueChange(property);
                }
            } else if (this.sortedElements[property] !== undefined) {
                if (this.sortedElements[property].checkValueChange) {
                    this.disableCheckValueChange(property);
                }

                delete this.sortedElements[property];
            }
        }
    };

    /**
    * Remove all elements.
    *
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    */
    MultipleElementsManager.prototype.clear = function () {
        var name;

        for (name in this.elements) {
            if (this.elements.hasOwnProperty(name)) {
                this.remove(name);
            }
        }
    };

    /**
    * Disable the check of value change on a property cached.
    *
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    */
    MultipleElementsManager.prototype.disableCheckValueChange = function (property) {
        var cache = this.sortedElements[property],
            i;

        if (cache !== undefined) {
            for (i = 0; i < cache.elements.length; i += 1) {
                this.removeCheckValueOn(property, cache.elements[i]);
            }


            this.sortedElements[property].checkValueChange = false;
        }
    };

    /**
    * Enable the check of value change on a property cached.
    *
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    */
    MultipleElementsManager.prototype.enableCheckValueChange = function (property) {
        var cache = this.sortedElements[property],
            i;

        if (cache !== undefined) {
            for (i = 0; i < cache.elements.length; i += 1) {
                this.addCheckValueOn(property, cache.elements[i]);
            }

            this.sortedElements[property].checkValueChange = true;
        }
    };

    /**
    * Get an element using its name.
    *
    * @this {MultipleElementsManager}
    * @param {String} name The name of the element you want to get.
    * @return {Object} The element or undefined if the element doesn't exist.
    */
    MultipleElementsManager.prototype.get = function (name) {
        return this.elements[name];
    };

    /**
    * Get all elements in a simple array.
    *
    * @this {MultipleElementsManager}
    * @return {Array} An simple array containing all elements.
    */
    MultipleElementsManager.prototype.getAll = function () {
        var array = [],
            name;

        for (name in this.elements) {
            if (this.elements.hasOwnProperty(name)) {
                array.push(this.elements[name]);
            }
        }

        return array;
    };

    /**
    * Get all elements in a sorted array.
    *
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property to use to sort the elements.
    * @param {Boolean} [ascending=true] If true, the sorting is by ascending order.
    * @return {Array} A simple array containing all elements sorted by the specified property, 
    *   or a none sorted array if the property doesn't exist or if the property type isn't sortable.
    */
    MultipleElementsManager.prototype.getAllSortedBy = function (property, ascending) {
        ascending = (typeof ascending !== 'boolean') ? true : ascending;
        var cache = this.sortedElements[property],
            elements;

        if (cache !== undefined) {
            if (!cache.checkValueChange) {
                elements = this.sortBy(cache.elements, property, cache.ascending);
            } else {
                if (cache.modified) {
                    elements = this.sortBy(cache.elements, '__' + property, cache.ascending);
                    cache.modified = false;
                } else {
                    elements = cache.elements;
                }
            }

            if (cache.ascending !== ascending) {
                cache.ascending = ascending;
                elements.reverse();
            }

            return elements;
        }

        return this.sortBy(this.getAll(), property, ascending);
    };

    /**
    * Get all elements in an associative array.
    *
    * @this {MultipleElementsManager}
    * @return {Array} An associative array containing all elements.
    */
    MultipleElementsManager.prototype.getAssiossiativeArray = function () {
        return this.elements;
    };

    /**
    * Get the name of an element.
    *
    * @this {MultipleElementsManager}
    * @param {Object} element The element to which you want to get its index.
    * @return {String} The name of the element or an empty string if the element doesn't exist.
    */
    MultipleElementsManager.prototype.getNameOf = function (element) {
        var name;

        for (name in this.elements) {
            if (this.elements.hasOwnProperty(name)) {
                if (this.elements[name] === element) {
                    return name;
                }
            }
        }

        return '';
    };

    /**
    * Remove an element using its name.
    * Its trigger the "element_removed" event whit the element name in parameter.
    *
    * @this {MultipleElementsManager}
    * @param {String} name The name of the element to remove.
    * @return {Boolean} The success of the removal.
    */
    MultipleElementsManager.prototype.remove = function (name) {
        if (this.elements.hasOwnProperty(name)) {
            var property;

            for (property in this.sortedElements) {
                if (this.sortedElements.hasOwnProperty(property)) {
                    if (this.sortedElements[property].elements.indexOf(this.elements[name]) >= 0) {
                        this.sortedElements[property].elements.splice(this.sortedElements[property].elements.indexOf(this.elements[name]), 1);
                    }

                    if (this.sortedElements[property].checkValueChange) {
                        this.removeCheckValueOn(property, this.elements[name]);
                    }
                }
            }

            delete this.elements[name];
            this.trigger('element_removed', name);

            return true;
        }

        return false;
    };

    /**
    * Disable the check of value change on a property of a particular element.
    *
    * @private
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    * @param {String} element The element witch disable check property value.
    */
    MultipleElementsManager.prototype.removeCheckValueOn = function (property, element) {
        Object.defineProperty(element, property, Object.getOwnPropertyDescriptor(element, '__' + property));
        delete element['__' + property];
    };

    /**
    * Remove an element.
    *
    * @this {MultipleElementsManager}
    * @param {Object} element The element to remove.
    * @return {Boolean} The success of the removal.
    */
    MultipleElementsManager.prototype.removeElement = function (element) {
        return this.remove(this.getNameOf(element));
    };

    /**
    * Sort a simple array.
    *
    * @this {MultipleElementsManager}
    * @param {Array} array The array to sort.
    * @param {String} property The name of the property to use to sort the elements.
    * @param {Boolean} [ascending=true] If true, the sorting is by ascending order.
    * @return {Array} A reference of the sorted array.
    */
    MultipleElementsManager.prototype.sortBy = function (array, property, ascending) {
        ascending = (typeof ascending !== 'boolean') ? true : ascending;

        if (array !== undefined) {
            array.sort(function (a, b) {
                if (a[property] > b[property]) {
                    if (ascending) {
                        return 1;
                    }

                    return -1;
                }
                if (a[property] < b[property]) {
                    if (ascending) {
                        return -1;
                    }

                    return 1;
                }

                return 0;
            });
            return array;
        }

        return [];
    };

    /**
    * Method called when a value of a caching property is modified.
    *
    * @private
    * @this {MultipleElementsManager}
    * @param {String} property The name of the property.
    */
    MultipleElementsManager.prototype.valueChanged = function (property) {
        this.sortedElements[property].modified = true;
    };

    return MultipleElementsManager;
});
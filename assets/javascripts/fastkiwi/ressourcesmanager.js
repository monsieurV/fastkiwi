/*global window, requirejs, require, define*/

define(['fastkiwi/eventshandler', 'fastkiwi/multipleelementsmanager'], function (EventsHandler, MultipleElementsManager) {
    'use strict';

    /**
     * Load, stock and manage ressources.
     *
     * @name RessourcesManager
     * @constructor
     * @augment EventsHandler
     * @this {RessourcesManager}
     */

    function RessourcesManager() {
        EventsHandler.apply(this);

        this.images     = new MultipleElementsManager();
        this.allImages  = []; //  ??? rename
        this.sounds     = new MultipleElementsManager();
        this.jsons      = new MultipleElementsManager();
        this.sprites    = new MultipleElementsManager();
        this.widgets    = new MultipleElementsManager();
        this.scripts    = new MultipleElementsManager();
    }

    RessourcesManager.prototype = Object.create(EventsHandler.prototype);

    RessourcesManager.prototype.autoload = function (callback) {
        this.loadJsonLoader('autoload', callback);
    };

    RessourcesManager.prototype.get = function (url, callback) {
        this.request('GET', url, null, callback);
    };

    RessourcesManager.prototype.getJson = function (name, callback) {
        var self = this,
            jsonData = this.jsons.get(name);

        if (jsonData === undefined) {
            this.get('jsons/' + name + '.json', function (response) {
                jsonData = JSON.parse(response);
                self.jsons.add(jsonData, name);

                if (typeof callback === 'function') {
                    callback(jsonData);
                }
            });
        } else if (typeof callback === 'function') {
            callback(jsonData);
        }
    };

    RessourcesManager.prototype.getXmlHttpRequest = function () {
        var xmlHttpRequest = null;

        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    xmlHttpRequest = new window.ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    xmlHttpRequest = new window.ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xmlHttpRequest = new window.XMLHttpRequest();
            }
        } else {
            return null;
        }

        return xmlHttpRequest;
    };

    RessourcesManager.prototype.loadImage = function (name, callback) {
        var self    = this,
            image   = this.images.get(name);

        if (image === undefined) {
            image = new window.Image();

            image.onload = function () {
                self.images.add(image, name);
                if (typeof callback === 'function') {
                    callback(image);
                }
            };

            image.src = './images/'  + name;
        } else if (typeof callback === 'function') {
            callback(image);
        }
    };

    RessourcesManager.prototype.loadImages = function(list, callback) {// tmp only one function needed !!
        var self = this;

        if (list !== undefined && list.length > 0) {
            this.loadImage(list.shift(), function() {
                self.loadImages(list, callback);
            });
        } else {
            callback();
        }
    }

    RessourcesManager.prototype.loadJson = function (name, callback) {
        this.getJson(name, callback);
    };

    RessourcesManager.prototype.loadJsons = function(list, callback) {// tmp only one function needed !!
        var self = this;

        if (list !== undefined && list.length > 0) {
            this.loadJson(list.shift(), function() {
                self.loadJsons(list, callback);
            });
        } else {
            callback();
        }
    }

    RessourcesManager.prototype.loadJsonLoader = function (name, callback) {
        var self = this;

        this.getJson('loaders/' + name, function (jsonData) {
            self.loadRessourcesFromJson(jsonData, callback);
        });
    };

    RessourcesManager.prototype.loadRessourcesFromJson = function (json, callback) { // tmp pas propre ...
        var self = this,
            type,
            i;

        //tmp
        this.loadJsons(json.jsons, function() {
            self.loadImages(json.images, function() {
                self.loadSounds(json.sounds, function() {
                    self.loadSprites(json.sprites, function() {
                        self.loadWidgets(json.widgets, function() {
                            callback();
                        });
                    })
                })
            });
        });
    };

    RessourcesManager.prototype.loadSound = function (name, callback) {
        //tmp to implement !!
        callback();
    };

    RessourcesManager.prototype.loadSounds = function(list, callback) {// tmp only one function needed !!
        var self = this;

        if (list !== undefined && list.length > 0) {
            this.loadSound(list.shift(), function() {
                self.loadSounds(list, callback);
            });
        } else {
            callback();
        }
    }

    RessourcesManager.prototype.loadSprite = function (name, callback) { // tmp add sprite and widget equivalent ....
        var self = this;

        this.getJson('sprites/' + name, function (sprite) {
            self.loadImage(sprite.image, function (image) {
                sprite.imageData = image;
                self.sprites.add(sprite, name);

                if (typeof callback === 'function') {
                    callback(sprite);
                }
            });
        });
    };

    RessourcesManager.prototype.loadSprites = function(list, callback) {// tmp only one function needed !!
        var self = this;

        if (list !== undefined && list.length > 0) {
            this.loadSprite(list.shift(), function() {
                self.loadSprites(list, callback);
            });
        } else {
            callback();
        }
    }

    RessourcesManager.prototype.loadWidget = function (type, name, callback) {
        var self = this;

        this.getJson('widgets/' + type + '/' + name, function (widget) {
            self.loadImage(widget.image, function (image) {
                widget.imageData = image;
                self.widgets.add(widget, type + '/' + name);
                if (typeof callback === 'function') {
                    callback(widget);
                }
            });
        });
    };

    RessourcesManager.prototype.loadButtons = function(list, callback) {// tmp only one function needed !!
        var self = this;

        if (list !== undefined && list.length > 0) {
            this.loadWidget("buttons", list.shift(), function() {
                self.loadButtons(list, callback);
            });
        } else {
            callback();
        }
    }

    RessourcesManager.prototype.loadWidgets = function(list, callback) {// tmp only one function needed !!
        var self = this;
        this.loadButtons(list["buttons"], function() {
            // self.loadTypeX(json.images, callback);//tmp ...
            callback();
        });
    }

    RessourcesManager.prototype.post = function (url, data, callback) {
        this.request('POST', url, data, callback);
    };

    RessourcesManager.prototype.request = function (method, url, data, callback) {
        var xmlHttpRequest = this.getXmlHttpRequest(),
            response;

        xmlHttpRequest.onreadystatechange = function () {
            if (xmlHttpRequest.readyState === 4) {
                if (xmlHttpRequest.status === 200) {
                    response = xmlHttpRequest.responseText;
                }

                if (typeof callback === 'function') {
                    callback(response);
                }
            }
        };

        xmlHttpRequest.open(method, url, true);
        xmlHttpRequest.send(data);
    };

    return RessourcesManager;
});